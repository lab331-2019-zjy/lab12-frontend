import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/service/course-service';
import Course from 'src/app/entity/course';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class CourseListComponent implements OnInit {

  courses: Course[];
  constructor(private courseService: CourseService) { }
  ngOnInit() {
    this.courseService.getCourses()
      .subscribe(courses => this.courses = courses);
    console.log(this.courses)
  }

}
