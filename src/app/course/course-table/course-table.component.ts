import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { CourseTableDataSource } from './course-table-datasource';
import Course from 'src/app/entity/course';
import { BehaviorSubject } from 'rxjs';
import { CourseService } from 'src/app/service/course-service';

@Component({
  selector: 'app-course-table',
  templateUrl: './course-table.component.html',
  styleUrls: ['./course-table.component.css']
})
export class CourseTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Course>;
  dataSource: CourseTableDataSource;
  loading: boolean;


  displayedColumns = ['id', 'courseId', 'courseName', 'content', 'lecturer'];
  courses: Course[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private courseService: CourseService) { }
  ngOnInit() {
    this.loading = true;
    this.courseService.getCourses()
      .subscribe(courses => {
        this.dataSource = new CourseTableDataSource();
        this.dataSource.data = courses;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.courses = courses;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        setTimeout(() => {
          this.loading = false;
        },500);
        console.log(this.courses[1]);
      }
      );
      
  }

  ngAfterViewInit() {

  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  
}
