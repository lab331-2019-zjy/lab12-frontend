import { Component, OnInit } from '@angular/core';
import Course from 'src/app/entity/course';
import { ActivatedRoute, Params } from '@angular/router';
import { CourseService } from 'src/app/service/course-service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(private route: ActivatedRoute, private courseService: CourseService) { }
  course: Course;
  ngOnInit(): void {
    this.route.params
      .subscribe((params: Params) => {
        this.courseService.getCourse(''+params['id'])
          .subscribe((inputCourse: Course) => {
          this.course = inputCourse
           
          }
          );
      });

  }
}
