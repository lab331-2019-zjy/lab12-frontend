import { Component } from '@angular/core';
import Course from '../../entity/course';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CourseService } from 'src/app/service/course-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course-add',
  templateUrl: './course.add.component.html',
  styleUrls: ['./course.add.component.css']
})
export class CourseAddComponent {
  course: Course;
 
 
  form = this.fb.group({
    id: [''],
    courseId: [null, Validators.compose([Validators.required, Validators.maxLength(6)])],
    courseName: [null, Validators.required],
    content: [null, Validators.required],
    lecturerName: [null, Validators.required],
    lecturerSurname: [null, Validators.required],
    
  });

  validation_messages = {
    'courseId': [
      { type: 'required', message: 'course id is required' },
      { type: 'maxlength', message: 'course id is too long' }
    ],
    'courseName': [
      { type: 'required', message: 'the course name is required' }
    ],
    'content': [
      { type: 'required', message: 'the course content is required' }
    ]
    ,
    'lecturerName': [
      { type: 'required', message: 'the lecturer\'s name is required' },
    ]
    ,
    'lecturerSurname': [
      { type: 'required', message: 'the lecturer\'s surname is required' },
    ]
  };

  ngOnInit(): void {
   
  }

  get diagnostic() {
    return JSON.stringify(this.form.value);
  };


  submit() {
    
    this.course = {
      id: null,
      courseId: this.form.value.courseId,
      courseName: this.form.value.courseName,
      content: this.form.value.content,
      lecturerName: this.form.value.lecturerName,
      lecturerSurname: this.form.value.lecturerSurname
    }
    this.courseService.saveCourse(this.course)
      .subscribe((course) => {
        this.router.navigate(['/courses']);
      }, (error) => {
        alert('could not save value');
      });
  }


  constructor(private fb: FormBuilder, private courseService: CourseService, private router: Router) {

  }
}
