import { Injectable } from '@angular/core';
import { CourseService } from './course-service';
import { HttpClient } from '@angular/common/http';
import Course from '../entity/course';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CourseRestImplService extends CourseService {
  saveCourse(course: Course): Observable<Course> {
    console.log(course);
    return this.http.post<Course>(environment.courseApi, course);
  }
  getCourse(id: any): Observable<Course> {
    return this.http.get<Course>(environment.courseApi + '/' + id);
  }
  
  getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(environment.courseApi);
  }

  constructor(private http: HttpClient) {
    super();
  }
}
