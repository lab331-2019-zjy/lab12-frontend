import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private http: HttpClient) {}
      uploadFile(image: File):Observable<any>{
        const formData: any = new FormData();
        formData.append('file', image);
        return this.http.post(environment.uploadApi, formData, {
          reportProgress: true,
          observe: 'events',
          responseType: 'text'
        }).pipe(
          catchError(this.errorMgmt)
        )
      }
       errorMgmt(error: HttpErrorResponse){
         let errorMessage = '';
         if(error.error instanceof ErrorEvent){
            errorMessage = error.error.message;
         }else{
           errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`
         }
         console.log(errorMessage);
         return throwError(errorMessage);
       }
}
