import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { CourseTableComponent } from './course/course-table/course-table.component';
import { CourseAddComponent } from './course/add/course.add.component';
import { InfoComponent } from './course/info/info.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  { path: 'courses', component: CourseTableComponent },
  { path: 'add-course', component: CourseAddComponent },
  { path: 'course/:id', component: InfoComponent },
  { path: '**', component: FileNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
