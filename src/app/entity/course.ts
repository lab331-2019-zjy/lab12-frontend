import Student from './student';
import Lecturer from './lecturer';

export default class Course {
    id: number;
    courseId: string;
    courseName: string;
    content: string;
    lecturerName: string;
    lecturerSurname: string
    // students: Student[]
  }
  